function [G, SUinv] = FRM(Z,nu);
% Calculates the frequency response matrix (FRM) starting from nu
% MIMO experiments using periodic signals. The inverse input matrix
% is also calculated.
%
%   [G, SUinv] = FRM(Z);
%   [G, SUinv] = FRM(Z,nu);
%
%	Output parameters
%
%       G       =   estimated frequency response matrix
%                   size ny x nu x F
%
%       SUinv	=   inverse of the input matrix inv(conj(U*U')) 
%                   size nu x nu x F
%
%
%	Input parameters
%
%       Z       =   matrix containing the output and input DFT spectra on top of each other of the nu MIMO experiments 
%                   size (ny+nu) x nu x F
%       nu      =   number of inputs of the system. This can be used when
%                   the amount of realisations is higher than the amount of inputs
%
%
% Rik Pintelon, October 21, 2009
% Adam Cooman, 02/06/2015 did a small change. You can define nu now, to
% allow more experiments than inputs


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialisation of the variables %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin==1
    [rows, nu, F] = size(Z);
    ny = rows - nu;
else
    [rows, ~, F] = size(Z);
    ny = rows - nu;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculation of the frequency response matrix %
% and the inverse input matrix                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

G = zeros(ny, nu, F);
SUinv = zeros(nu, nu, F);
if nu == 1
    G(:, 1, :) = Z(1:ny, 1, :) ./ repmat(Z(end, 1, :), [ny, 1, 1]);
    SUinv(1, 1, :) = 1./squeeze(abs(Z(end, 1, :).^2));
else % nu > 1
    for kk = 1:F
        SUinv(:, :, kk) = pinv(conj(squeeze(Z((ny+1):end, :, kk)) * squeeze(Z((ny+1):end, :, kk))'));% also changed inv into pinv here
        G(:, :, kk) = Z(1:ny, :, kk) / (squeeze(Z((ny+1):end, :, kk)));
    end % kk
end % if one input
